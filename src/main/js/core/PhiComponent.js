import PhiTemplate from "./PhiTemplate";
import Processor from './processor/Processor';

/**
 * @class CoreComponent
 * @description This class is the core component for phi framework.
 */

export default class PhiComponent {

    /**
     *
     * @param {*} id unique id of the component.
     */
    constructor( id ) {

        this.id = id;
        this.element = null;
        this.properties = {};

        window.addEventListener( PhiComponent.EVENT_VALUE_CHANGED , this.resetProperties.bind(this) );
    }

    /**
     * @description This method is called when the set value is triggered from any controller.
     */
    resetProperties() {
        //TODO: Optimize this
        this.setProperties();
        this.setTemplate();
        this.setListeners();

    }


    get ID() {
        return this.id;
    }

    /**
     *
     * @param target event target of the html component
     * This method sets the properties of the PhiComponent.
     */
    setProperties() {

        const component = Object.create( this.element );
        let props = {};
        Object.keys(component.__proto__).forEach((prop) => {
            props[prop] = component.__proto__[prop];
        });

        this.properties = props;

    }

    /**
     *
     * @param target event target of the html component
     * This method is used for setting the template
     */
    setTemplate() {
        //console.log(11);
        if(!this.template) this.template = new PhiTemplate( this.element.templateUri );
        this.template.fetchTemplateFromUrl(( markup ) => {

            console.log(markup);
            //let templateWithProps = this.template.getRenderingHTML(markup.firstChild.innerHTML , this.properties);
            //this.element.renderTemplate(templateWithProps);
            //this.setListeners();

        });

    }

    /**
     * @description This method is used to add event listeners corresponding to the functions in the controller.
     */
    setListeners() {

        this.element.shadowRoot.querySelectorAll('[phi-click]')
            .forEach(( el ) => {

                const functionName = el.getAttribute('phi-click');

                el.addEventListener('click', () => {
                    this.element[functionName].call(this.element);
                });

            });

        this.element.onLoaded();

    }


    processMarkers() {
        //console.log(this.element);

        if(!this.template) this.template = new PhiTemplate( this.element.templateUri );

        this.template.fetchTemplateFromUrl(( markup ) => {

            let container = PhiTemplate.getContainer();
            let processor = new Processor( markup,  container);
            processor.process();

        });

    }



}

PhiComponent.EVENT_CONNECTED      = 'componentconnected';
PhiComponent.EVENT_DISCONNECTED   = 'componentdisconnected';
PhiComponent.EVENT_VALUE_CHANGED  = 'componentpropertychanged'