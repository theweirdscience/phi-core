import PhiComponent from './PhiComponent';

/**
 * @class PhiComponentFactory
 * @description This classes manages the components created by the user
 */
export default class PhiComponentFactory {

    constructor() {

        this.components = {};
        this.noOfComponents = 0;

        window.addEventListener( PhiComponent.EVENT_CONNECTED , this.handleComponentCreated.bind( this ) );
    }


    /**
     *
     * @param {Event} e
     * @description listening to component's created callback
     * Here
     */
    handleComponentCreated( e ) {

        let componentKey = e.target.constructor.name;

        let phiComponent = this.addToFactory( componentKey );
        phiComponent.element = e.target;
        phiComponent.processMarkers();
        //phiComponent.setTemplate();

    }


    /**
     *
     * @param componentKey The id for each component in the component factory
     * @returns {*} the PhiComponent instance
     */
    addToFactory( componentKey ) {

        if(!this.components[ componentKey ]) {
            this.components[ componentKey ] = new PhiComponent( componentKey );
        }
        this.noOfComponents++;
        return this.components[ componentKey ];

    }

    /**
     *
     * @param {*} clazz
     * @description This method register the custom elements to the component list for further data-binding
     * and other dom manipulation processes.
     */
    register( clazz ) {

        window.customElements.define( this.enforceLowerCaseDashes( clazz.name ), clazz );
        this.addToFactory(clazz.name);

    }


    /**
     * TODO : improve this
     * @param name
     * @returns {string}
     */
    enforceLowerCaseDashes( name ) {
        return `phi-${name.split(/(?=[A-Z])/).join('-').toLowerCase()}`;
    }

}