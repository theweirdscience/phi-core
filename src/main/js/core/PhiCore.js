import ComponentFactory from "./PhiComponentFactory";
/**
 * @class PhiCore
 * @description Starting point for the framework
 */
export class PhiCore {
    
    constructor() {
        this.componentFactory = new ComponentFactory();
    }

    /**
     * 
     * @description Static method to instantiate the framwork with array of component classes defined by user
     */
    static register( /* components */ ) {

        if ( !this.componentFactory ) {
            this.componentFactory = new ComponentFactory();
        }
        
        let components = arguments;
        //console.log('hello', arguments);
        Array.prototype.map.call( components, component => {
            this.componentFactory.register( component ); 
        } );

    }

}
