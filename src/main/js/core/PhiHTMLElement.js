import PhiComponent from './PhiComponent';
/**
 * @class PhiHTMLElement
 * @description PhiHTMLElement class for managing the html compoennt and its updation
 */
export class PhiHTMLElement extends HTMLElement {


    constructor() {
        super();
        //this.renderer = new PhiHTMLElementRenderer( this );
    }


    connectedCallback() {
        this.dispatchEvent( new Event( PhiComponent.EVENT_CONNECTED, { bubbles: true } ) );
    }

    disconnectedCallback() {
        this.dispatchEvent( new Event( PhiComponent.EVENT_DISCONNECTED, { bubbles: true } ) );
    }

    attributeChangedCallback( name, oldValue, newValue, namespaceURI ) {
        console.log('ac', name, oldValue, newValue, namespaceURI )
        this.dispatchEvent( new Event( PhiComponent.EVENT_VALUE_CHANGED, { bubbles: true } ) );
    }


    /**
     * @description this method is used to update the dom using Mustache template engine to map properties
     */
    renderTemplate(html) {

        let shadowRoot = ( this.shadowRoot ) ? this.shadowRoot : this.createShadowRoot();
        shadowRoot.innerHTML = html;

    }

    /**
     *
     * @param key property key to update
     * @param value property value to update
     * @description This method is used to set the properties of the controller
     * and dispatch a event for UI changes
     */
    setValue( key, value ) {

        if( this[key] ) {
            this[key] = value;
        }

        this.dispatchEvent( new Event( PhiComponent.EVENT_VALUE_CHANGED, { bubbles: true } ) );

    }

    /**
     * @description This method can be overridden to execute operations when dom is loaded.
     */
    onLoaded() {

    }

}



