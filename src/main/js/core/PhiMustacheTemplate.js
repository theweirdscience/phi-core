import Mustache from 'mustache';

/**
 * @class PhiMustacheTemplate
 * @description class for initializing and managing the templates using mustache
 */
export default class PhiMustacheTemplate {

    constructor( url ) {

        this.url  = url;
        this.initTemplate();

	}

    /**
     * @description This method is used to initialize the template
     */
    initTemplate() {

        this.node = document.createElement('script');
        this.node.setAttribute("type", "x-tmpl-mustache");

    }

    /**
     *
     * @description This function is used to fetch the template from url and parse it using Mustache.
     */
    parseTemplate( cb ) {

        fetch( this.url )
            .then(( response ) => {
                    if ( response.status !== 200 ) {
                        console.log('Looks like there was a problem. Status Code: ' +
                            response.status);
                        return;
                    }

                    response.text().then( ( template ) => {

                        Mustache.parse(template);
                        const temp = document.createElement('div');
                        temp.innerHTML = template;
                        this.node.appendChild(temp);
                        cb(this.node);

                    });
                }
            )
            .catch((err) => {
                console.log('Fetch Error :-S', err);
            });

    }

    /**
     *
     * @param html html without properties
     * @param properties properties
     * @return Mustached rendered template with properties
     */
    getRenderingHTML( html, properties ) {
        return Mustache.render( html , properties );
    }

}