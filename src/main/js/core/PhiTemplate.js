/**
 * @class PhiTemplate
 * @description class for initializing and managing the templates
 */
export default class PhiTemplate {

    constructor( url ) {

        this.url  = url;
        //this.phiTemplateEngine = new Template();
        this.initTemplate();
	}

    /**
     * @description This method is used to initialize the template
     */
    initTemplate() {

        this.node = document.createElement('script');
        this.node.setAttribute("type", "x-tmpl-phi");

    }

    static getContainer() {

        let node = document.createElement('div');
        node.setAttribute("phi-id", "final-component");
        return node;

    }

    /**
     *
     * @description This function is used to fetch the template from url and parse it using Mustache.
     */
    fetchTemplateFromUrl ( cb ) {

        fetch( this.url )
            .then(( response ) => {
                    if ( response.status !== 200 ) {
                        console.log('Looks like there was a problem. Status Code: ' +
                            response.status);
                        return;
                    }

                    response.text().then( ( template ) => {

                        const temp = document.createElement('div');
                        temp.innerHTML = template;
                        this.node.appendChild(temp);
                        // this returns pml parsed html
                        cb(this.node);


                    });
                }
            )
            .catch((err) => {
                console.log('Fetch Error :-S', err);
            });

    }

    /**
     *
     * @param html html without properties
     * @param properties properties
     * @return Mustached rendered template with properties
     */
    getRenderingHTML( html, properties ) {
        //return Mustache.render( html , properties );
    }

}