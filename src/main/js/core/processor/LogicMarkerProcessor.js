
export default class LogicForProcessor {

    constructor() {

    }

    /**
     *
     * process
     * evaluate
     * isValid
     *
     */

    /**
     * Where the magic happens...
     */
    scan( component, virtual ) {

        let selector = '[\\(logic\\:for\\)]';

        let nodes = component.template.querySelectorAll( selector );
        nodes = nodes.filter( ( node ) => { return node.querySelector( `:not(${selector})` ) } ); // filter out any children with a for loop, reserve for later processing.
        nodes.map( ( node ) => { this.process( component, virtual, node ) } );

        return virtual;

    }


    process( component, virtual, node ) {

        let expression = this.evaluate( node );
        expression.collection.map( ( value ) => { this.produce( component, virtual, node ) } );

    }

    evaluate( node ) {
/*
        let result;
        //let expression = ( node.getAttribute( '(logic:for)' );

        if ( this.isValid( expression ) ) {
            let tuple = expression.split( 'in' );
            result = { key : tuple[ 0 ].trim(), collection: this.component[ tuple[ 1 ].trim() ] } ];
        }

        return result;
*/
    }

    isValid( expression ) {

        let result = true;

        // TODO : validate expression

        return result;

    }

    produce( value, parent ) {

        let element = document.createElement( node.tagName );
        parent.appendChild( element );

    }

}