import LogicMarkerProcessor from './LogicMarkerProcessor';


export default class Processor {

    constructor( component, virtual ) {

        this.component = component.firstChild;
        this.virtual = virtual;

        this.logic = new LogicMarkerProcessor();
        this.nodes = [];
        this.noOfNodes = 0;

    }

    process() {
        this.getNodes( this.component );
    }


    getNodes( component ) {

        if( component.hasChildNodes() ) {
            component.childNodes.forEach( this.checkNode.bind(this) );
        }
    }

    checkNode( node ) {

        if( node instanceof HTMLElement) {
            this.parseNode( node );
        }
    }

    parseNode( node ) {

        if( this.hasMarkers( node ) ) {
            node = this.parseNodeMarkers( node );
        }

        if( node.hasChildNodes() ) {
            this.getNodes( node );
        } else {
            return node;
        }
        //return node;
    }

    hasMarkers( node ) {

        return ( node.hasAttribute('(logic:for)') || node.hasAttribute('(logic:if)') );
    }

    parseNodeMarkers( node ) {
        console.log(node);
        return node;
    }



}